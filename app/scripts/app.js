(function (angular) {

    'use strict';
    
    /**
     * ============================================
     * Yugioh Master Module.
     * Everything else must be declared as a
     * dependency within this module.
     * --------------------------------------------
     * @module YugiohApp
     * --------------------------------------------
     */
    angular.module('YugiohApp', [
        'ngRoute',
        'ngSanitize',
        'controllers.home',
        'controllers.details',
        'controllers.error',
        'filters.formatTitle',
        'services.rest'
    ])

    /**
     * ======================================================
     * Configure Application
     * ======================================================
     */
    .config(['$routeProvider', '$httpProvider', '$locationProvider', function ($routeProvider, $httpProvider, $locationProvider) {
        /**
         * ================================================
         * @class {RouteProvider}
         * ================================================
         */
            /**
             * Configure application routing
             */
            $routeProvider
                .when('/', {
                    templateUrl: '/views/home.html',
                    controller: 'HomeController',
                })
                .when('/error', {
                    templateUrl: '/views/error.html',
                    controller: 'ErrorController',
                })
                .when('/:card', {
                    templateUrl: '/views/details.html',
                    controller: 'DetailsController',
                })
                .otherwise({
                    redirectTo: '/error'
                });

        $locationProvider.html5Mode(true);

    }]) //end application configuration

    .controller('AppCtrl', ['$rootScope', '$scope', '$location', function($rootScope, $scope, $location){
        
    }]);

})(window.angular);
