'use strict';

/*
 * Used to replace _ in card details data keys, to be user friendly on display
 */

angular.module('filters.formatTitle', [])

.filter('formatTitle', function() {
    return function (title){
		return title.replace("_", " ");;
    }
});