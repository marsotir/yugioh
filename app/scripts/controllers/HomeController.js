'use strict';
/*
 * Home page controler to create main deck
 */

var homeCtrl = angular.module('controllers.home', []);

homeCtrl.controller('HomeController', [
    '$scope',
    '$http',
    '$anchorScroll',

    function ($scope, $http, $anchorScroll) {
        $anchorScroll();

        /*
		 * Get data from local json file to create the home page deck
         */
        $http.get('/data/cards.json').
			then(function(response) {
		    	$scope.cards = response.data
		  	}).
		  	catch(function onError(response) {
		   		console.log(response);
		  	});

		/*
		 * Set filter value if user clicks on a type name to fiter deck
         */
		$scope.filterType = function(type){
			$scope.filterTo = type;
		}

		/*
		 * Reset filter value
         */
		$scope.resetFilter = function(){
			$scope.filterTo = "";
		}
    }
]);


