'use strict';

/*
 * Details Controller used to get and display card data
 */

var DetailsCtrl = angular.module('controllers.details', []);

DetailsCtrl.controller('DetailsController', [
    '$scope',
    '$routeParams',
    '$anchorScroll',
    '$location',
    'CardDetails',

    function ($scope, $routeParams, $anchorScroll, $location, CardDetails) {
        $anchorScroll();
        $scope.cardDetails = null;

        /*
         * Get path param and request data from the api
         * If success save card details to param, otherwise redirect to error page 
         */
        CardDetails.getDetails({card: $routeParams.card}, function(res){
        	$scope.cardDetails = res.data;
        }, function(error) {
		    $location.path("/error");
		})
    }
]);


