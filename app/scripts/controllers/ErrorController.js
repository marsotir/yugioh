'use strict';

/*
 * Error page controller
 */

var ErrorCtrl = angular.module('controllers.error', []);

ErrorCtrl.controller('ErrorController', [
    '$scope',
    '$anchorScroll',

    function ($scope, $anchorScroll) {
        $anchorScroll();
    }
]);


