(function (angular) {

    'use strict';

    var restApi = angular.module('services.rest', ['ngResource']);
    var host = 'http://52.57.88.137';

    /*
     * Call to the api to get card details
     */
    restApi.factory('CardDetails', ['$resource', function ($resource) {
        return $resource(host+'/api/card_data/:card', '', {
            getDetails: {
                method: 'GET',
                isArray: false
            }
        });
    }]);

})(window.angular);
