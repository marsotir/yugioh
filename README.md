# README #

Simple Yu-Gi-Oh! Deck Browser

There is a home page where you may find all available cards, filter cards by type if you click on a card type, and see card details if you click on a card title or image

### Instalaltion ###
* Clone repo 
* Run `npm install | bower install` to install all dependencies
* Run `grunt build` to build the app and use files from ./dist folder
